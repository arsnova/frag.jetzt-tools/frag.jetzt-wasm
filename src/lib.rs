
use wasm_bindgen::{__rt::WasmRefCell, prelude::*, throw_str};
use word_cloud_placing::{
    quadtree::{AxisAlignedBoundingBox, QuadTree},
    word_cloud_algorithm::place_elements,
    word_cloud_elements::{WordCloudElement, WordCloudState},
};

mod word_cloud_placing;

#[wasm_bindgen]
pub fn do_something(data: &str) -> String {
    let mut result = String::from("Hello ");
    result.push_str(data);
    result
}

/// Calculates positions based on input data.
///
/// ```no_run
/// Input data: [
///   width: f32, height: f32,
///   obstacle_count: f32,
///   {obstacle_center_x: f32, obstacle_center_y: f32, obstacle_width: f32, obstacle_height: f32}[obstacle_count],
///   {element_width: f32, element_height: f32, rotation: f32}...
/// ]
///
/// Output data: [
///   ... skipped data (info, obstacles),
///   {r: f32, phi: f32, rotation: f32}...
/// ]
/// ```
/// rotation will not be changed
#[wasm_bindgen]
pub fn calc_word_cloud(data: &mut [f32]) {
    let width = data[0];
    let height = data[1];
    let aspect_ratio = height / width;
    let half_width = width / 2.0;
    let halh_height = height / 2.0;
    let mut quad_tree = QuadTree::<WasmRefCell<WordCloudElement>>::new(
        AxisAlignedBoundingBox::new(-half_width, -halh_height, width, height),
        0,
    );
    let obstacle_count = data[2] as usize;
    let mut k = 3;
    let mut elements: Vec<WasmRefCell<WordCloudElement>> = vec![];
    for _ in 0..obstacle_count {
        elements.push(WasmRefCell::new(WordCloudElement::from_pos(
            data[k] - half_width,
            data[k + 1] - halh_height,
            data[k + 2],
            data[k + 3],
        )));
        k += 4;
    }
    let size = (data.len() - k) / 3;
    for _ in 0..size {
        elements.push(WasmRefCell::new(WordCloudElement::new(
            data[k],
            data[k + 1],
            data[k + 2],
        )));
        k += 3;
    }
    for index in 0..obstacle_count {
        let elem = &elements[index];
        let c = elem.borrow();
        quad_tree.add_element(elem, |x| {
            let WordCloudState::Placed(pos) = c.get_state() else { throw_str("Obstacle should be placed!") };
            x.intersects(pos.get_right_range(), pos.get_up_range())
        });
    }
    place_elements(&mut quad_tree, &elements, aspect_ratio, obstacle_count);
    let mut index = 3 + obstacle_count * 4;
    for x in obstacle_count..elements.len() {
        let elem = &elements[x].borrow();
        if let WordCloudState::Placed(pos) = elem.get_state() {
            let pos = pos.get_position();
            data[index] = (pos.get_x() * pos.get_x() + pos.get_y() * pos.get_y()).sqrt(); // r
            data[index + 1] = pos.get_y().atan2(pos.get_x()).to_degrees(); // phi
        } else {
            data[index] = -1.0;
            data[index + 1] = -1.0;
        }
        index += 3;
    }
}

#[cfg(test)]
pub mod tests {
    use wasm_bindgen_test::wasm_bindgen_test;

    use super::*;

    #[wasm_bindgen_test]
    pub fn test_word_cloud() {
        let mut test = [
            1920.0, 1080.0, // width x height
            1.0,    // obstacle count
            500.0, 15.0, 200.0, 30.0, // obstacle 1, center_x, center_y, width, height
            100.0, 300.0, 45.0, // element 1, width, height, rotation
            250.0, 150.0, 0.0, // element 2
            500.0, 16.0, 5.0, // element 3
        ];
        calc_word_cloud(&mut test);
        assert_all_valid(&test);
    }

    #[wasm_bindgen_test]
    pub fn test_simple_case() {
        let mut test = [
            1820.0, 950.0, // width x height
            0.0,   // obstacle count
            88.0625, 40.0, 0.0, // element 1, width, height, rotation
            73.3594, 40.0, 0.0, // element 2
            85.3281, 40.0, 0.0, // element 3
            84.0, 40.0, 0.0, // element 4
            215.578, 40.0, 0.0, // element 5
            234.734, 40.0, 0.0, // element 6
            86.7031, 40.0, 0.0, // element 7
            285.266, 40.0, 0.0, // element 8
            67.5625, 40.0, 0.0, // element 9
            161.375, 40.0, 0.0, // element 10
            105.3281, 40.0, 0.0, // element 11
            130.703, 40.0, 0.0, // element 12
            312.016, 40.0, 0.0, // element 13
            182.062, 40.0, 0.0, // element 14
            151.547, 40.0, 0.0, // element 15
            153.312, 40.0, 0.0, // element 16
            198.656, 40.0, 0.0, // element 17
            97.3438, 40.0, 0.0, // element 18
            186.734, 40.0, 0.0, // element 19
            90.6875, 40.0, 0.0, // element 20
        ];
        calc_word_cloud(&mut test);
        assert_all_valid(&test);
    }

    fn assert_all_valid(data: &[f32]) {
        let start = (data[2] as usize) * 4 + 3;
        let count = (data.len() - start) / 3;
        let mut index = start;
        for _ in 0..count {
            assert!(data[index] >= 0.0);
            index += 3;
        }
    }
}
