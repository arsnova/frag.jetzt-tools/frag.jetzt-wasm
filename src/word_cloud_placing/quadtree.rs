use super::word_cloud_elements::{Range, Vector};

type Float = f32;

pub struct AxisAlignedBoundingBox {
    x_range: Range,
    y_range: Range,
}

impl AxisAlignedBoundingBox {
    pub fn new(x: Float, y: Float, width: Float, height: Float) -> Self {
        Self {
            x_range: Range::new(x, x + width),
            y_range: Range::new(y, y + height),
        }
    }

    pub fn intersects(&self, x_range: &Range, y_range: &Range) -> bool {
        self.x_range.intersects(x_range) && self.y_range.intersects(y_range)
    }

    pub fn get_points(&self) -> [Vector; 4] {
        [
            Vector::new(self.x_range.get_start(), self.y_range.get_start()),
            Vector::new(self.x_range.get_start(), self.y_range.get_end()),
            Vector::new(self.x_range.get_end(), self.y_range.get_start()),
            Vector::new(self.x_range.get_end(), self.y_range.get_end()),
        ]
    }
}

pub struct QuadTree<'a, T> {
    bounds: AxisAlignedBoundingBox,
    children: Option<Box<[QuadTree<'a, T>; 9]>>,
    elements: Vec<&'a T>,
    depth: u8,
}

pub trait AABBCollision: Fn(&AxisAlignedBoundingBox) -> bool + Copy {}
impl<T> AABBCollision for T where T: Fn(&AxisAlignedBoundingBox) -> bool + Copy {}

pub trait ObjectCollision<T>: Fn(&T) -> bool + Copy {}
impl<T, K> ObjectCollision<K> for T where T: Fn(&K) -> bool + Copy {}

impl<'a, T> QuadTree<'a, T> {
    pub fn new(bounds: AxisAlignedBoundingBox, depth: u8) -> Self {
        Self {
            bounds,
            children: None,
            elements: vec![],
            depth,
        }
    }

    /// Queries objects in the quadtree
    ///
    /// This method queries for elements in this quadtree based on the child_collide function.
    /// The function returns true if it was exited early.
    ///
    ///  * `child_collide` - the function to check if a children quadtree is involved in the current collision
    ///  * `on_collide` - the function to run, when querying elements of a quadtree. When it returns true, the query exits early
    pub fn query<F: AABBCollision, C: ObjectCollision<T>>(
        &self,
        child_collide: F,
        on_collide: C,
    ) -> bool {
        for element in self.elements.iter() {
            if !on_collide(*element) {
                return true;
            }
        }
        if let Some(children) = self.children.as_ref() {
            for child in (*children).iter() {
                if child_collide(&child.bounds) && child.query(child_collide, on_collide) {
                    return true;
                }
            }
        }
        false
    }

    pub fn add_element<F: AABBCollision>(&mut self, t: &'a T, child_collide: F) {
        if self.depth < 4 && self.children.is_none() && self.elements.len() >= 4 {
            self.create_children();
            for elem in self.elements.drain(..).collect::<Vec<_>>() {
                self.add_element(elem, child_collide);
            }
        }
        // when we have children
        if let Some(children) = self.children.as_mut() {
            let mut child_index: Option<usize> = None;
            let mut overriden = false;
            // search for best fit children
            for child in (*children).iter().enumerate() {
                if child_collide(&child.1.bounds) {
                    if let Some(_) = child_index {
                        overriden = true;
                        break;
                    }
                    child_index = Some(child.0)
                }
            }
            // if more or none, add to parent
            if overriden || child_index.is_none() {
                self.elements.push(t);
                return;
            }
            // if exact one, at to that child
            children[child_index.unwrap()].add_element(t, child_collide);
            return;
        }
        self.elements.push(t);
    }

    fn create_children(&mut self) {
        let x = self.bounds.x_range.get_start();
        let y = self.bounds.y_range.get_start();
        let width = (self.bounds.x_range.get_end() - x) / 3.0;
        let height = (self.bounds.y_range.get_end() - y) / 3.0;
        let x1 = x + width;
        let x2 = x1 + width;
        let y1 = y + height;
        let y2 = y1 + height;
        let depth = self.depth + 1;
        self.children = Some(Box::new([
            Self::new(AxisAlignedBoundingBox::new(x, y, width, height), depth),
            Self::new(AxisAlignedBoundingBox::new(x1, y, width, height), depth),
            Self::new(AxisAlignedBoundingBox::new(x2, y, width, height), depth),
            Self::new(AxisAlignedBoundingBox::new(x, y1, width, height), depth),
            Self::new(AxisAlignedBoundingBox::new(x1, y1, width, height), depth),
            Self::new(AxisAlignedBoundingBox::new(x2, y1, width, height), depth),
            Self::new(AxisAlignedBoundingBox::new(x, y2, width, height), depth),
            Self::new(AxisAlignedBoundingBox::new(x1, y2, width, height), depth),
            Self::new(AxisAlignedBoundingBox::new(x2, y2, width, height), depth),
        ]));
    }
}
