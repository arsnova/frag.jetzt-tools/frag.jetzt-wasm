use std::mem::{replace, swap};

use wasm_bindgen::throw_str;

type Float = f32;

#[derive(Default, Debug, Clone)]
pub struct Range {
    start: Float,
    end: Float,
}

impl Range {
    pub fn new(start: Float, end: Float) -> Self {
        Self { start, end }
    }

    pub fn from_projected(projection: &Vector, points: &[Vector]) -> Self {
        let mut min = points[0].dot(projection);
        let mut max = min;
        for x in points {
            let value = x.dot(projection);
            min = if value < min { value } else { min };
            max = if value > max { value } else { max };
        }
        Self {
            start: min,
            end: max,
        }
    }

    pub fn add(mut self, value: Float, end: Float) -> Self {
        self.start += value;
        self.end += value + end;
        self
    }

    pub fn get_end(&self) -> Float {
        self.end
    }

    pub fn get_start(&self) -> Float {
        self.start
    }

    pub fn intersects(&self, range: &Range) -> bool {
        self.start < range.end && self.end > range.start
    }
}

#[derive(Default, Debug, Clone)]
pub struct RangeSet {
    ranges: Vec<Range>,
}

impl RangeSet {
    pub fn new() -> Self {
        Self {
            ranges: vec![Range::new(0.0, 2.0)],
        }
    }

    pub fn is_empty(&self) -> bool {
        self.ranges.is_empty()
    }

    pub fn clear(&mut self) {
        self.ranges.clear()
    }

    pub fn get_optimal(&self, optimal: Float) -> Float {
        let mut min = self.ranges[0].start;
        for range in self.ranges.iter() {
            if optimal < range.start {
                if (min - optimal).abs() > (range.start - optimal).abs() {
                    min = range.start;
                }
                break;
            } else if optimal <= range.end {
                return optimal;
            }
            min = range.end;
        }
        min
    }

    pub fn add_collision(&mut self, range: &Range) {
        if range.end <= 0.0 || range.start >= 2.0 {
            return;
        }
        let mut to_add: Option<Range> = None;
        let mut index = 0;
        self.ranges.retain_mut(|r| {
            if to_add.is_none() {
                index += 1;
            }
            if range.start <= r.start {
                if range.end <= r.start {
                    return true;
                } else if range.end < r.end {
                    // adjust start
                    r.start = range.end;
                    return true;
                }
                // delete
                return false;
            }
            if range.start >= r.end {
                return true;
            } else if range.end >= r.end {
                // adjust end
                r.end = range.start;
            } else {
                to_add = Some(Range::new(range.end, r.end));
                r.end = range.start;
            }
            true
        });
        if let Some(r) = to_add {
            self.ranges.insert(index, r);
        }
    }
}

#[derive(Default, Debug, Clone)]
pub struct Vector {
    x: Float,
    y: Float,
}

const DIR_X: Vector = Vector { x: 1.0, y: 0.0 };
const DIR_Y: Vector = Vector { x: 1.0, y: 0.0 };

impl Vector {
    pub fn new(x: Float, y: Float) -> Self {
        Self { x, y }
    }

    pub fn add(mut self, vec: &Vector) -> Self {
        self.x += vec.x;
        self.y += vec.y;
        self
    }

    pub fn sub(mut self, vec: &Vector) -> Self {
        self.x -= vec.x;
        self.y -= vec.y;
        self
    }

    pub fn mult(mut self, factor: Float) -> Self {
        self.x *= factor;
        self.y *= factor;
        self
    }

    pub fn dot(&self, vec: &Vector) -> Float {
        self.x * vec.x + self.y * vec.y
    }

    pub fn get_x(&self) -> Float {
        self.x
    }

    pub fn get_y(&self) -> Float {
        self.y
    }

    pub fn calculate_optimal(&self, dir_vector: &Vector, aspect_ratio: Float) -> Float {
        let x = dir_vector.x * aspect_ratio;
        let y = dir_vector.y;
        let a = self.x * aspect_ratio;
        let b = self.y;
        -(a * x + b * y) / (x * x + y * y)
    }

    pub fn len_squared(&self, aspect_ratio: Float) -> Float {
        let x = self.x * aspect_ratio;
        let y = self.y;
        x * x + y * y
    }
}

///```no_run
/// .
///            ^ n1 (dir1 = height / 2)
///  P2 _______|_______ P1
///     |             |
///     |             |- >  n0 (dir0 = width / 2)
///     |             |
///  P3 --------------- P0
/// .
///```
/// Fields:
///   - `n0`: When rotation = 0, is the unit vector (1 0)
///   - `n1`: When rotation = 0, is the unit vector (0 1)
///   - `dir0`: `n0` scaled by width / 2
///   - `dir1`: `n1` scaled by height / 2
///   - `vec_p`: vector of points without offset, for index see above [P0, P1, P2, P3]
///   - `range0`: is the projected range from this rect projected onto `n0`. It has no offset, so that this is [-width / 2, width / 2]
///   - `range1`: is the projected range from this rect projected onto `n1`. It has no offset, so that this is [-height / 2, height / 2]
///   - `projected_x`: is the projected range from this rect projected onto unit vector (1 0) - or in other words, the x component. This is used for quadtree collisions
///   - `projected_y`: is the projected range from this rect projected onto unit vector (0 1) - or in other words, the y component. This is used for quadtree collisions
#[derive(Debug, Default, Clone)]
pub struct BuildInfo {
    n0: Vector,
    n1: Vector,
    dir0: Vector,
    dir1: Vector,
    vec_p: [Vector; 4],
    range0: Range,
    range1: Range,
    projected_x: Range,
    projected_y: Range,
}

impl BuildInfo {
    fn new(width: Float, height: Float, rotation_deg: Float) -> Self {
        let (sin, cos) = rotation_deg.to_radians().sin_cos();
        let width = width / 2.0;
        let height = height / 2.0;
        let n0 = Vector::new(cos, sin);
        let n1 = Vector::new(-sin, cos);
        let dir0 = n0.clone().mult(width);
        let dir1 = n1.clone().mult(height);
        let points = [
            dir0.clone().sub(&dir1),
            dir0.clone().add(&dir1),
            dir1.clone().sub(&dir0),
            Vector::default().sub(&dir0).sub(&dir1),
        ];
        Self {
            n0,
            n1,
            dir0,
            dir1,
            projected_x: Range::from_projected(&DIR_X, &points),
            projected_y: Range::from_projected(&DIR_Y, &points),
            vec_p: points,
            range0: Range::new(-width, width),
            range1: Range::new(-height, height),
        }
    }

    pub fn get_point(&self, index: usize) -> &Vector {
        &self.vec_p[index]
    }

    pub fn get_points(&self) -> &[Vector] {
        &self.vec_p
    }

    pub fn get_n_up(&self) -> &Vector {
        &self.n1
    }

    pub fn get_n_right(&self) -> &Vector {
        &self.n0
    }

    pub fn get_up(&self) -> &Vector {
        &self.dir1
    }

    pub fn get_right(&self) -> &Vector {
        &self.dir0
    }

    pub fn get_projected_x(&self) -> &Range {
        &self.projected_x
    }

    pub fn get_projected_y(&self) -> &Range {
        &self.projected_y
    }

    pub fn get_up_range(&self) -> &Range {
        &self.range1
    }

    pub fn get_right_range(&self) -> &Range {
        &self.range0
    }
}

/// A placed position is more or less a BuildInfo struct placed at position (x y)
///
/// Fields:
///   - `n0`: See BuildInfo
///   - `n1`: See BuildInfo
///   - `dir0`: See BuildInfo
///   - `dir1`: See BuildInfo
///   - `vec_p`: See BuildInfo, offset with (x y)
///   - `range0`: See BuildInfo, offset with (x y)
///   - `range1`: See BuildInfo, offset with (x y)
///   - `pos`: Center position
#[derive(Debug, Default, Clone)]
pub struct PlacedPosition {
    n0: Vector,
    n1: Vector,
    dir0: Vector,
    dir1: Vector,
    vec_p: [Vector; 4],
    range0: Range,
    range1: Range,
    projected_x: Range,
    projected_y: Range,
    pos: Vector,
}

impl PlacedPosition {
    pub fn new(x: Float, y: Float, width: Float, height: Float) -> Self {
        let width = width / 2.0;
        let height = height / 2.0;
        let start_x = x - width;
        let end_x = x + width;
        let start_y = y - height;
        let end_y = y + height;
        Self {
            n0: DIR_X.clone(),
            n1: DIR_Y.clone(),
            dir0: DIR_X.clone().mult(width),
            dir1: DIR_Y.clone().mult(height),
            vec_p: [
                Vector::new(end_x, start_y),
                Vector::new(end_x, end_y),
                Vector::new(start_x, end_y),
                Vector::new(start_x, start_y),
            ],
            range0: Range::new(start_x, end_x),
            range1: Range::new(start_y, end_y),
            projected_x: Range::new(start_x, end_x),
            projected_y: Range::new(start_y, end_y),
            pos: Vector::new(x, y),
        }
    }

    pub fn from_info(pos: Vector, mut info: BuildInfo) -> Self {
        let dot0 = pos.dot(&info.n0);
        let dot1 = pos.dot(&info.n1);
        let mut point = Vector::default(); // point = default
        swap(&mut info.vec_p[0], &mut point); // point = p0
        point = point.add(&pos);
        swap(&mut info.vec_p[0], &mut point); // point = default
        swap(&mut info.vec_p[1], &mut point); // point = p1
        point = point.add(&pos);
        swap(&mut info.vec_p[1], &mut point); // point = default
        swap(&mut info.vec_p[2], &mut point); // point = p2
        point = point.add(&pos);
        swap(&mut info.vec_p[2], &mut point); // point = default
        swap(&mut info.vec_p[3], &mut point); // point = p3
        point = point.add(&pos);
        info.vec_p[3] = point;
        Self {
            n0: info.n0,
            n1: info.n1,
            dir0: info.dir0,
            dir1: info.dir1,
            vec_p: info.vec_p,
            range0: info.range0.add(dot0, 0.0),
            range1: info.range1.add(dot1, 0.0),
            projected_x: info.projected_x.add(pos.x, 0.0),
            projected_y: info.projected_y.add(pos.y, 0.0),
            pos,
        }
    }

    pub fn get_point(&self, index: usize) -> &Vector {
        &self.vec_p[index]
    }

    pub fn get_points(&self) -> &[Vector] {
        &self.vec_p
    }

    pub fn get_n_up(&self) -> &Vector {
        &self.n1
    }

    pub fn get_n_right(&self) -> &Vector {
        &self.n0
    }

    pub fn get_up(&self) -> &Vector {
        &self.dir1
    }

    pub fn get_right(&self) -> &Vector {
        &self.dir0
    }

    pub fn get_projected_x(&self) -> &Range {
        &self.projected_x
    }

    pub fn get_projected_y(&self) -> &Range {
        &self.projected_y
    }

    pub fn get_up_range(&self) -> &Range {
        &self.range1
    }

    pub fn get_right_range(&self) -> &Range {
        &self.range0
    }

    pub fn get_position(&self) -> &Vector {
        &self.pos
    }
}

#[derive(Debug)]
pub enum WordCloudState {
    Placed(PlacedPosition),
    Unplaced(BuildInfo),
}

#[derive(Debug)]
pub struct WordCloudElement {
    rotation: Float,
    state: WordCloudState,
}

impl WordCloudElement {
    pub fn new(width: Float, height: Float, rotation_deg: Float) -> Self {
        Self {
            rotation: rotation_deg,
            state: WordCloudState::Unplaced(BuildInfo::new(width, height, rotation_deg)),
        }
    }

    pub fn from_pos(x: Float, y: Float, width: Float, height: Float) -> Self {
        Self {
            rotation: 0.0,
            state: WordCloudState::Placed(PlacedPosition::new(x, y, width, height)),
        }
    }

    /// Calculates rotation index for collision testing
    ///
    /// rotIndex == 0: rotation + 0..90 = other.rotation (e. g. rotation = 45, other.rotation = 134) <br />
    /// with p0 -> other.p2, p1 -> other.p3, p2 -> other.p0, p3 -> other.p1
    ///
    /// rotIndex == 1: rotation + 90..180 = other.rotation (e. g. rotation = 30, other.rotation = 180) <br />
    /// with p0 -> other.p1, p1 -> other.p2, p2 -> other.p3, p3 -> other.p0
    ///
    /// rotIndex == 2: rotation + 180..270 = other.rotation (e. g. rotation = 15, other.rotation = 200) <br />
    /// with p0 -> other.p0, p1 -> other.p1, p2 -> other.p2, p3 -> other.p3
    ///
    /// rotIndex == 3: rotation + 270..360 = other.rotation (e. g. rotation = 340, other.rotation = 270) <br />
    /// with p0 -> other.p3, p1 -> other.p0, p2 -> other.p1, p3 -> other.p2
    pub fn get_rotation_index(&self, other_rotation: Float) -> usize {
        let value = ((other_rotation - self.rotation) / 90.0) % 4.0;
        if value < 0.0 {
            value as usize + 4
        } else {
            value as usize
        }
    }

    /// Gets difference between 90°
    pub fn get_rotation_diff(&self, other: &WordCloudElement) -> Float {
        let value = ((self.rotation - other.rotation) % 90.0).abs();
        if value > 45.0 {
            return 90.0 - value;
        } else {
            value
        }
    }

    pub fn get_rotation(&self) -> Float {
        self.rotation
    }

    pub fn get_state(&self) -> &WordCloudState {
        &self.state
    }

    pub fn update_with_pos(&mut self, pos: Vector) {
        let old = replace(
            &mut self.state,
            WordCloudState::Unplaced(BuildInfo::default()),
        );
        let WordCloudState::Unplaced(info) = old else { throw_str("Tried to place an already placed element!") };
        self.state = WordCloudState::Placed(PlacedPosition::from_info(pos, info));
    }
}
