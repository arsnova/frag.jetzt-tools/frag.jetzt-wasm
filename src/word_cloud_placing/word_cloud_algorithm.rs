use wasm_bindgen::{__rt::WasmRefCell, throw_str};

use super::{
    quadtree::QuadTree,
    word_cloud_elements::{
        BuildInfo, PlacedPosition, Range, RangeSet, Vector, WordCloudElement, WordCloudState,
    },
};

type Float = f32;

/// Vector position, distance squared
type PossiblePosition = (Vector, Float);

#[derive(Debug)]
enum Collision {
    Always,
    Never,
    Ranged(Range),
}

pub fn place_elements<'a>(
    tree: &mut QuadTree<'a, WasmRefCell<WordCloudElement>>,
    elements: &'a Vec<WasmRefCell<WordCloudElement>>,
    aspect_ratio: Float,
    start_index: usize,
) {
    {
        let elem = &elements[start_index];
        let mut e = elem.borrow_mut();
        // check with obstacles if (0, 0) is free, else place around obstacle
        e.update_with_pos(Vector::new(0.0, 0.0));
        let WordCloudState::Placed(pos) = &e.get_state() else { throw_str("place_elements: must be placed!") };
        tree.add_element(elem, |aabb| {
            aabb.intersects(pos.get_projected_x(), pos.get_projected_y())
        })
    }
    let mut last_dist: Float = 0.0;
    for x in start_index + 1..elements.len() {
        let reference = &elements[x];
        let mut elem = reference.borrow_mut();
        let WordCloudState::Unplaced(info) = elem.get_state() else { throw_str("place_elements: must be unplaced!") };
        let start = find_best(elements, start_index, x, info, last_dist, aspect_ratio);
        for index in start..x {
            let placed = &elements[index].borrow();
            let result = place(tree, placed, &elem, aspect_ratio);
            if let Some((pos, pos_dist)) = result {
                elem.update_with_pos(pos);
                let WordCloudState::Placed(pos) = elem.get_state() else { throw_str("place_elements: must be placed!") };
                tree.add_element(reference, |aabb| {
                    aabb.intersects(pos.get_projected_x(), pos.get_projected_y())
                });
                last_dist = pos_dist;
                break;
            }
        }
        let WordCloudState::Placed(_) = elem.get_state() else { throw_str("place_elements: No place found!") };
    }
}

fn find_best(
    elements: &Vec<WasmRefCell<WordCloudElement>>,
    start_index: usize,
    end_index: usize,
    info: &BuildInfo,
    last_dist: Float,
    aspect_ratio: Float,
) -> usize {
    let add_x = info.get_projected_x().get_end();
    let add_y = info.get_projected_y().get_end();
    let mut min = start_index;
    let mut min_dist = Float::MAX;
    for x in start_index..end_index {
        let elem = &elements[x].borrow();
        let WordCloudState::Placed(pos) = elem.get_state() else { throw_str("find_best: must be placed!") };
        let x_range = pos.get_projected_x();
        let max_x = (x_range.get_start().abs().max(x_range.get_end()) + add_x) * aspect_ratio;
        let y_range = pos.get_projected_y();
        let max_y = y_range.get_start().abs().max(y_range.get_end()) + add_y;
        let dist = max_x * max_x + max_y * max_y;
        let dist = (last_dist - dist).abs();
        if dist < min_dist {
            min = x;
            min_dist = dist;
        }
    }
    min
}

/// This constant must be <= 45°
const TOLERANCE_ANGLE: Float = 1.0;

fn place(
    tree: &QuadTree<WasmRefCell<WordCloudElement>>,
    placed: &WordCloudElement,
    to_place: &WordCloudElement,
    aspect_ratio: Float,
) -> Option<PossiblePosition> {
    let WordCloudState::Placed(pos) = placed.get_state() else { throw_str("place: Should be placed!") };
    let WordCloudState::Unplaced(to_place_info) = to_place.get_state() else { throw_str("place: Should be unplaced!") };
    if placed.get_rotation_diff(to_place) <= TOLERANCE_ANGLE {
        let rotation_index = placed.get_rotation_index(to_place.get_rotation() + 45.0);
        place_four_sides(tree, pos, to_place_info, rotation_index, aspect_ratio)
    } else {
        let rotation_index = placed.get_rotation_index(to_place.get_rotation());
        place_eight_sides(tree, pos, to_place_info, rotation_index, aspect_ratio)
    }
}

fn place_four_sides(
    tree: &QuadTree<WasmRefCell<WordCloudElement>>,
    pos: &PlacedPosition,
    to_place_info: &BuildInfo,
    rotation_index: usize,
    aspect_ratio: Float,
) -> Option<PossiblePosition> {
    let mut rotation_index = (6 - rotation_index) % 4;
    let mut minimum: Option<PossiblePosition> = None;
    for x in 0..4 {
        let start = pos
            .get_point(x)
            .clone()
            .sub(to_place_info.get_point(rotation_index));
        let dir_first = match x {
            0 => pos.get_up().clone(),
            1 => pos.get_right().clone().mult(-1.0),
            2 => pos.get_up().clone().mult(-1.0),
            3 => pos.get_right().clone(),
            _ => throw_str("Invalid index?"),
        };
        let dir_second = match rotation_index {
            0 => to_place_info.get_up().clone().mult(-1.0),
            1 => to_place_info.get_right().clone(),
            2 => to_place_info.get_up().clone(),
            3 => to_place_info.get_right().clone().mult(-1.0),
            _ => throw_str("invalid rotation index?"),
        };
        let dir = dir_first.add(&dir_second);
        let optimal_t = start.calculate_optimal(&dir, aspect_ratio);
        let result = test_side(tree, &start, &dir, to_place_info, optimal_t);
        calculate_min(&mut minimum, result, aspect_ratio);
        rotation_index = (rotation_index + 1) % 4;
    }
    minimum
}

fn place_eight_sides(
    tree: &QuadTree<WasmRefCell<WordCloudElement>>,
    pos: &PlacedPosition,
    to_place_info: &BuildInfo,
    rotation_index: usize,
    aspect_ratio: Float,
) -> Option<PossiblePosition> {
    let mut rotation_index = (6 - rotation_index) % 4;
    let mut minimum: Option<PossiblePosition> = None;
    for x in 0..4 {
        let start = pos
            .get_point(x)
            .clone()
            .sub(to_place_info.get_point(rotation_index));
        let dir = match x {
            0 => pos.get_up().clone(),
            1 => pos.get_right().clone().mult(-1.0),
            2 => pos.get_up().clone().mult(-1.0),
            3 => pos.get_right().clone(),
            _ => throw_str("Invalid index?"),
        };
        let optimal_t = start.calculate_optimal(&dir, aspect_ratio);
        let result = test_side(tree, &start, &dir, to_place_info, optimal_t);
        calculate_min(&mut minimum, result, aspect_ratio);
        let start = start.add(&dir.mult(2.0));
        let dir = match rotation_index {
            0 => to_place_info.get_up().clone().mult(-1.0),
            1 => to_place_info.get_right().clone(),
            2 => to_place_info.get_up().clone(),
            3 => to_place_info.get_right().clone().mult(-1.0),
            _ => throw_str("invalid rotation index?"),
        };
        let optimal_t = start.calculate_optimal(&dir, aspect_ratio);
        let result = test_side(tree, &start, &dir, to_place_info, optimal_t);
        calculate_min(&mut minimum, result, aspect_ratio);
        rotation_index = (rotation_index + 1) % 4;
    }
    minimum
}

fn calculate_min(
    minimum: &mut Option<PossiblePosition>,
    result: Option<Vector>,
    aspect_ratio: Float,
) {
    if let Some(pos) = result {
        let len = pos.len_squared(aspect_ratio);
        let new_min = Some((pos, len));
        if let Some(current_min) = &minimum {
            if len < current_min.1 {
                *minimum = new_min;
            }
        } else {
            *minimum = new_min;
        }
    }
}

fn test_side(
    tree: &QuadTree<WasmRefCell<WordCloudElement>>,
    start_point: &Vector,
    dir_vector: &Vector,
    info: &BuildInfo,
    optimal_t: Float,
) -> Option<Vector> {
    let x_range = info
        .get_projected_x()
        .clone()
        .add(start_point.get_x(), 2.0 * dir_vector.get_x());
    let y_range = info
        .get_projected_y()
        .clone()
        .add(start_point.get_y(), 2.0 * dir_vector.get_y());
    let up = info.get_n_up();
    let right = info.get_n_right();
    let up_range = info.get_up_range().clone().add(up.dot(start_point), 0.0);
    let right_range = info
        .get_right_range()
        .clone()
        .add(right.dot(start_point), 0.0);
    let up_len = up.dot(dir_vector);
    let up_zero = up_len.abs() < Float::EPSILON;
    let right_len = right.dot(dir_vector);
    let right_zero = right_len.abs() < Float::EPSILON;
    let is_aabb = up.get_x().abs() <= Float::EPSILON || up.get_y().abs() <= Float::EPSILON;
    let collisions = WasmRefCell::new(RangeSet::new());
    tree.query(
        |aabb| {
            let aabb_intersects = aabb.intersects(&x_range, &y_range);
            if is_aabb || !aabb_intersects {
                return aabb_intersects;
            }
            let points = aabb.get_points();
            if !info
                .get_up_range()
                .intersects(&Range::from_projected(up, &points))
            {
                return false;
            }
            info.get_right_range()
                .intersects(&Range::from_projected(right, &points))
        },
        |cell| {
            let elem = cell.borrow();
            let WordCloudState::Placed(pos) = elem.get_state() else { throw_str("on_collide: Should be placed!") };
            // own up
            let mut collision = collide_range(
                &up_range,
                up_len,
                up_zero,
                &Range::from_projected(up, pos.get_points()),
            );
            if let Collision::Never = collision {
                return false;
            }
            // own right
            let new_collision = collide_range(
                &right_range,
                right_len,
                right_zero,
                &Range::from_projected(right, pos.get_points()),
            );
            collision = minify(collision, new_collision);
            if let Collision::Never = collision {
                // TODO: Cut here if angles are nearly equal ((right * other.right).abs() + (right * other.up).abs()) >= 1 - eps
                return false;
            }
            // other up
            let up = pos.get_n_up();
            let up_len = up.dot(dir_vector);
            let up_zero = up_len.abs() < Float::EPSILON;
            let new_collision = collide_range(
                &Range::from_projected(up, info.get_points()),
                up_len,
                up_zero,
                pos.get_up_range(),
            );
            collision = minify(collision, new_collision);
            if let Collision::Never = collision {
                return false;
            }
            // other right
            let right = pos.get_n_right();
            let right_len = right.dot(dir_vector);
            let right_zero = right_len.abs() < Float::EPSILON;
            let new_collision = collide_range(
                &Range::from_projected(right, info.get_points()),
                right_len,
                right_zero,
                pos.get_right_range(),
            );
            collision = minify(collision, new_collision);
            if let Collision::Never = collision {
                return false;
            }
            let mut collision_set = collisions.borrow_mut();
            if let Collision::Always = collision {
                collision_set.clear();
            } else {
                let Collision::Ranged(range) = collision else { throw_str("No Collision!") };
                collision_set.add_collision(&range);
            }
            return collision_set.is_empty();
        },
    );
    // Check optimal
    let set = collisions.into_inner();
    if set.is_empty() {
        return None;
    }
    let optimal_value = set.get_optimal(optimal_t);
    let mut best_pos = start_point.clone();
    best_pos = best_pos.add(&dir_vector.clone().mult(optimal_value));
    return Some(best_pos);
}

/// collision1 is never Collision::Never
fn minify(collision1: Collision, collision2: Collision) -> Collision {
    if let Collision::Always = collision1 {
        return collision2;
    }
    if let Collision::Always = collision2 {
        return collision1;
    }
    if let Collision::Never = collision2 {
        return collision2;
    }
    let Collision::Ranged(range1) = collision1 else { throw_str("Collision 1 should be ranged.") };
    let Collision::Ranged(range2) = collision2 else { throw_str("Collision 2 should be ranged.") };
    let start = range1.get_start().max(range2.get_start());
    let end = range1.get_end().min(range2.get_end());
    if start >= end {
        return Collision::Never;
    }
    return Collision::Ranged(Range::new(start, end));
}

fn collide_range(
    move_range: &Range,
    move_len: f32,
    move_zero: bool,
    static_range: &Range,
) -> Collision {
    if move_zero {
        if move_range.intersects(static_range) {
            Collision::Always
        } else {
            Collision::Never
        }
    } else {
        let v1 = (static_range.get_start() - move_range.get_end()) / move_len;
        let v2 = (static_range.get_end() - move_range.get_start()) / move_len;
        Collision::Ranged(Range::new(v1.min(v2), v1.max(v2)))
    }
}
