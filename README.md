# Compile Rust Wasm

```bash
wasm-pack build

wasm-pack build --target web # only older versions
```

# Test Website

```bash
cd test
bun run index.ts
```

Open [http://localhost:3000](http://localhost:3000)