const debug = true;

class Box {
  constructor(width, height, rotation) {
    this.width = width;
    this.widthHalf = width / 2;
    this.height = height;
    this.heightHalf = height / 2;
    this.rotation = radians(rotation);
    this.updateRotation();
  }

  updateRotation() {
    this.n0 = p5.Vector.fromAngle(this.rotation);
    this.n1 = createVector(-this.n0.y, this.n0.x);
    this.dir0 = this.n0.copy().mult(this.widthHalf);
    this.dir1 = this.n1.copy().mult(this.heightHalf);
  }
  
  draw() {
    push();
    rotate(this.rotation);
    rect(-this.widthHalf, -this.heightHalf, this.width, this.height);
    if (debug) {
      // vec 0
      line(this.widthHalf, 0, this.widthHalf + 15, 0);
      line(this.widthHalf + 10, -5, this.widthHalf + 15, 0);
      line(this.widthHalf + 10, 5, this.widthHalf + 15, 0);
      // vec 1
      line(0, this.heightHalf, 0, this.heightHalf + 15);
      line(-5, this.heightHalf + 10, 0, this.heightHalf + 15);
      line(5, this.heightHalf + 10, 0, this.heightHalf + 15);
    }
    pop();
  }
  
  drawLines(box) {
    let other0 = box.dir0;
    let other1 = box.dir1;
    let diff = ((box.rotation - this.rotation) * 2 / Math.PI) % 4;
    if (diff < 0) diff += 4;
    diff = Math.trunc(diff);
    if (diff === 1) {
      //other0 = other0.copy().mult(-1);
      //other1 = other1.copy().mult(-1);
    }
    console.log(diff);
    /*
           ^ n1
  2 _______|_______ 1
    |             |
    |             |- >  n0
    |             |
  3 --------------- 0

    rotIndex = 0: 
     0 -> 2, 1 -> 3, 2 -> 0, 3 -> 1
    rotIndex = 1:
     0 -> 1, 1 -> 2, 2 -> 3, 3 -> 0
    rotIndex = 2:
     0 -> 0, 1 -> 1, 2 -> 2, 3 -> 3
    rotIndex = 3:
     0 -> 3, 1 -> 0, 2 -> 1, 3 -> 2

    */
    const p1 = this.dir0.copy().sub(this.dir1).add(other0).sub(other1);
    const p2 = p1.copy().add(this.dir1.copy().mult(2));
    line(p1.x, p1.y, p2.x, p2.y);
    const p3 = p2.copy().add(other1.copy().mult(2));
    line(p2.x, p2.y, p3.x, p3.y);
    const p4 = p3.copy().sub(this.dir0.copy().mult(2));
    line(p3.x, p3.y, p4.x, p4.y);
    const p5 = p4.copy().sub(other0.copy().mult(2));
    line(p4.x, p4.y, p5.x, p5.y);
    const p6 = p5.copy().sub(this.dir1.copy().mult(2));
    line(p5.x, p5.y, p6.x, p6.y);
    const p7 = p6.copy().sub(other1.copy().mult(2));
    line(p6.x, p6.y, p7.x, p7.y);
    const p8 = p7.copy().add(this.dir0.copy().mult(2));
    line(p7.x, p7.y, p8.x, p8.y);
    line(p8.x, p8.y, p1.x, p1.y);
    stroke(255);
    strokeWeight(5);
    point(p1);
    point(p2);
    point(p3);
    point(p4);
    point(p5);
    point(p6);
    point(p7);
    point(p8);
    stroke(0);
    strokeWeight(1);
  }
}

let box, box2;
const RAD_PER_COUNT = Math.PI / 180;

function setup() {
  createCanvas(400, 400);
  box = new Box(50, 100, 0);
  box2 = new Box(150, 40, -45);
  console.log(box);
}

function draw() {
  background(220);
  // translate to center + flip y
  applyMatrix(1, 0, 0, -1, width / 2, height / 2);
  box.draw();
  box2.draw();
  box.drawLines(box2);
  if (box2.rotation > Math.PI / 2) {
    noLoop();
  }
  if (frameCount % 2 == 0) {
    box2.rotation += RAD_PER_COUNT * 2;
    box2.updateRotation();
  }
}