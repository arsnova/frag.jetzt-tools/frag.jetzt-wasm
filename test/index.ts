const server = Bun.serve({
    port: 3000,
    development: true,
    fetch(request) {
      const url = new URL(request.url);
      let pathname = url.pathname;
      if (pathname.length < 2) {
        pathname = '/index.html';
      }
      if (pathname === '/favicon.ico') {
        return new Response(`404!`);
      }
      pathname = pathname.substring(1);
      if (pathname.startsWith('pkg/')) {
        pathname = '../pkg/' + pathname.substring(4);
      }
      console.log(pathname + "<--");
      return new Response(Bun.file(pathname));
    },
  });
  
  console.log(`Listening on localhost:${server.port}`);